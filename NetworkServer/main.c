#include <stdio.h>
#include <string.h>
#include <gst/gst.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>

#define BASEPORT 61000
#define MAXCLIENTS 10
pthread_t threads[MAXCLIENTS];

typedef struct Watcher {
	char*	ip;
	int	port;
	int	clientindex;	// which client has opened this watcher
	GstElement	*vidsink;
	GstElement	*audsink;
} Watcher;

typedef struct Clientinfo {
	int		index;
	int		fd;
	char		*ip;
	int		iswearable;  // 1 if wearableui, 0 if serverui
	char*	name;
	int		vidport;	// local ports for receiving video and audio
	int		audport;
	GstElement	*vidsrc;  // receiving elements for audio and video
	GstElement	*audsrc;

	Watcher	watchers[4];

	GstElement	*vidpipe;
	GstElement	*audpipe;

	GstElement	*tee;
	GstPad		*pads[4]; //split out to a maximum 4 clients, I guess
	GstElement	*queues[4]; // We need these apparently

	GstElement	*audtee;
	GstPad		*audpads[4];
	GstElement	*audqueues[4];

	GstElement	*fakepad;
	GstPad		*fakeaudpad;
	GstElement	*fakequeue;
	GstElement	*fakeaudqueue;
	GstElement	*fakesink;
	GstElement	*fakeaudsink;
	GstElement	*fakesinks[4];
	GstElement	*fakeaudsinks[4];
	
} Clientinfo;

Clientinfo *clients[MAXCLIENTS];
int serversock;

int
Readline(int sockd, void *vptr, int maxlen)
{
	int n, rc;
	char c, *buffer;

	buffer = vptr;

	for (n = 1; n < maxlen; n++) {
		if ((rc = read(sockd, &c, 1)) == 1) {
			if (c != '\r')
				*buffer++ = c;
			if (c == '\n') {
				break;
			}
		} else if (rc == 0) {
			if (n == 1)
				return 0;
			else
				break;
		} else {
			if (errno = EINTR)
				continue;
			return -1;
		}
	}
	*buffer = 0;
	return n;
}

int
Writeline(int sockd, const void *vptr, int n) {
	int nleft, nwritten;
	const char *buffer;

	buffer = vptr;
	nleft = n;

	while (nleft > 0) {
		if ((nwritten = write(sockd, buffer, nleft)) <= 0) {
			if (errno == EINTR)
				nwritten = 0;
			else
				return -1;
		}
		nleft -= nwritten;
		buffer += nwritten;
	}
	return n;
}

void
initgst(Clientinfo *c) {
	int i;
	char name[32];

	c->vidpipe = gst_pipeline_new("videoPipeline");
	c->audpipe = gst_pipeline_new("audioPipeline");
	c->vidsrc = gst_element_factory_make("udpsrc", "video-source");
	c->audsrc = gst_element_factory_make("udpsrc", "audio-source");
	c->tee = gst_element_factory_make("tee", "video-splitter");
	c->audtee = gst_element_factory_make("tee", "audio-splitter");
	for (i = 0; i < 4; i++) {
		sprintf(name, "queue%d", i);
		c->queues[i] = gst_element_factory_make("queue", name);
		c->pads[i] = gst_element_get_request_pad(c->tee, "src%d");
		gst_bin_add_many(GST_BIN(c->vidpipe), c->queues[i], NULL);

		sprintf(name, "audqueue%d", i);
		c->audqueues[i] = gst_element_factory_make("queue", name);
		c->audpads[i] = gst_element_get_request_pad(c->audtee, "src%d");
		gst_bin_add_many(GST_BIN(c->audpipe), c->audqueues[i], NULL);
//		sprintf(name, "fakesink%d", i);
//		c->fakesinks[i] = gst_element_factory_make("fakesink", name);
//		gst_bin_add(GST_BIN(c->vidpipe), c->fakesinks[i]);
	}
	c->fakequeue = gst_element_factory_make("queue", "fakequeue");
	c->fakeaudqueue = gst_element_factory_make("queue", "fakeaudqueue");
	c->fakesink = gst_element_factory_make("fakesink", "fakesink");
	c->fakeaudsink = gst_element_factory_make("fakesink", "fakeaudsink");
	g_object_set(G_OBJECT(c->vidsrc), "port", (gint)c->vidport, NULL);
	g_object_set(G_OBJECT(c->audsrc), "port", (gint)c->audport, NULL);

	gst_bin_add_many(GST_BIN(c->vidpipe), c->vidsrc, c->tee, c->fakequeue, c->fakesink, NULL);
	gst_bin_add_many(GST_BIN(c->audpipe), c->audsrc, c->audtee, c->fakeaudqueue, c->fakeaudsink, NULL);

	c->fakepad = gst_element_get_request_pad(c->tee, "src%d");
	c->fakeaudpad = gst_element_get_request_pad(c->audtee, "src%d");
	gst_element_link_many(c->vidsrc, c->tee, c->fakequeue, c->fakesink, NULL);
	gst_element_link_many(c->audsrc, c->audtee, c->fakeaudqueue, c->fakeaudsink, NULL);

	gst_element_set_state(c->vidpipe, GST_STATE_PLAYING);
	gst_element_set_state(c->audpipe, GST_STATE_PLAYING);
}

/* Close client[cnum]'s watcher #wnum */
void
closesink(int cnum, int wnum)
{
	char name[32];
	Clientinfo *c = clients[cnum];
	gst_element_set_state(c->vidpipe, GST_STATE_NULL);
	gst_element_set_state(c->audpipe, GST_STATE_NULL);
	printf("unlink stuff\n");
	gst_element_unlink_many(c->tee, c->queues[wnum], c->watchers[wnum].vidsink, NULL);
	gst_element_unlink_many(c->audtee, c->audqueues[wnum], c->watchers[wnum].audsink, NULL);
	printf("removing vidsink from pipe\n");
	gst_bin_remove(GST_BIN(c->vidpipe), c->watchers[wnum].vidsink);
	gst_bin_remove(GST_BIN(c->audpipe), c->watchers[wnum].audsink);
//	if (c->fakesinks[wnum] == NULL) {
		printf("creating new fakesinks\n");;
		sprintf(name, "fakesink%d", wnum);
		c->fakesinks[wnum] = gst_element_factory_make("fakesink", name);
		c->fakeaudsinks[wnum] = gst_element_factory_make("fakesink", name);
//	}
	printf("adding fakesink to pipeline\n");
	gst_bin_add(GST_BIN(c->vidpipe), c->fakesinks[wnum]);
	gst_bin_add(GST_BIN(c->audpipe), c->fakeaudsinks[wnum]);
	printf("linking fakesinks\n");
	gst_element_link_many(c->tee, c->queues[wnum], c->fakesinks[wnum], NULL);
	gst_element_link_many(c->audtee, c->audqueues[wnum], c->fakeaudsinks[wnum], NULL);
	memset(&c->watchers[wnum], 0, sizeof(Watcher));
	printf("restarting pipeline\n");
	gst_element_set_state(c->vidpipe, GST_STATE_PLAYING);
	gst_element_set_state(c->audpipe, GST_STATE_PLAYING);
}

/* Clean up when a client is ready to close */
void
cleanupclient(Clientinfo *c)
{
	int i, j;
	printf("Cleaning up user index %d, ip=%s\n", c->index, c->ip);
	for (i = 0; i < MAXCLIENTS; i++) {
		for (j = 0; j < 4; j++) {
//			printf("j = %d\n", j);
			if (clients[i]->watchers[j].port != 0) {
				printf("checking against watcher ip=%s port=%d\n", clients[i]->watchers[j].ip, clients[i]->watchers[j].port);
				if (clients[i]->watchers[j].clientindex == c->index) {
					printf("closing client %d's watcher number %d\n", i, j);
					closesink(i, j);
					printf("closed\n");
				}
			}
		}
	}


	if (c->iswearable == 1)
		for (i = 0; i < 4; i++) {
			if (c->watchers[i].ip != NULL) {
				closesink(c->index, i);
			}
		}

	c->fd = 0;
	c->ip = NULL;
	c->iswearable = 0;
	c->name = NULL;
	c->vidport = c->audport = 0;
	gst_element_set_state(c->vidpipe, GST_STATE_NULL);
	gst_element_set_state(c->audpipe, GST_STATE_NULL);

	printf("cleaned up client\n\n");

	return;
	gst_element_unlink_many(c->vidsrc, c->tee, c->queues[0], c->queues[1], c->queues[2], c->queues[3], c->fakesink[0], c->fakesink[1], c->fakesink[2], c->fakesink[3], c->fakepad, c->fakequeue, c->fakesink, NULL);
	gst_element_unlink_many(c->audsrc, c->audtee, c->audqueues[0], c->audqueues[1], c->audqueues[2], c->audqueues[3], c->fakeaudsink[0], c->fakeaudsink[1], c->fakeaudsink[2], c->fakeaudsink[3], c->fakeaudpad, c->fakeaudqueue, c->fakeaudsink, NULL);
	gst_bin_remove_many(GST_BIN(c->vidpipe), c->vidsrc, c->tee, c->queues[0], c->queues[1], c->queues[2], c->queues[3], c->fakesink[0], c->fakesink[1], c->fakesink[2], c->fakesink[3], c->fakepad, c->fakequeue, c->fakesink, NULL);
	gst_bin_remove_many(GST_BIN(c->audpipe), c->audsrc, c->audtee, c->audqueues[0], c->audqueues[1], c->audqueues[2], c->audqueues[3], c->fakeaudsink[0], c->fakeaudsink[1], c->fakeaudsink[2], c->fakeaudsink[3], c->fakeaudpad, c->fakeaudqueue, c->fakeaudsink, NULL);
//	gst_element_set_state(c->vidpipe, GST_STATE_PLAYING);
//	gst_element_set_state(c->audpipe, GST_STATE_PLAYING);
}

void
newsink(int cnum, int wnum, int index, int port, char *ip) {
	char name[32];
	Clientinfo *c = clients[cnum];

	c->watchers[wnum].clientindex = index;
	c->watchers[wnum].port = port;
	c->watchers[wnum].ip = ip;
	sprintf(name, "vidsink%d", wnum);
	printf("Adding view #%d to client #%d's stream, on %s!%d\n", wnum, cnum, ip, port);
	gst_element_set_state(c->vidpipe, GST_STATE_NULL);
	gst_element_set_state(c->audpipe, GST_STATE_NULL);
	if (c->fakesinks[wnum] != NULL) {
		gst_element_unlink_many(c->tee, c->queues[wnum], c->fakesinks[wnum], NULL);
		gst_element_unlink_many(c->audtee, c->audqueues[wnum], c->fakeaudsinks[wnum], NULL);
		gst_bin_remove(GST_BIN(c->vidpipe), c->fakesinks[wnum]);
		gst_bin_remove(GST_BIN(c->audpipe), c->fakeaudsinks[wnum]);
	}
	c->watchers[wnum].vidsink = gst_element_factory_make("udpsink", name);
	sprintf(name, "audsink%d", wnum);
	c->watchers[wnum].audsink = gst_element_factory_make("udpsink", name);
	g_object_set(G_OBJECT(c->watchers[wnum].vidsink), "host", ip, "port", port, NULL);
	g_object_set(G_OBJECT(c->watchers[wnum].audsink), "host", ip, "port", port+1, NULL);
	gst_bin_add_many(GST_BIN(c->vidpipe), c->watchers[wnum].vidsink, NULL);
	gst_bin_add_many(GST_BIN(c->audpipe), c->watchers[wnum].audsink, NULL);
	gst_element_link_many(c->tee, c->queues[wnum], c->watchers[wnum].vidsink, NULL);
	gst_element_link_many(c->audtee, c->audqueues[wnum], c->watchers[wnum].audsink, NULL);
	gst_element_set_state(c->vidpipe, GST_STATE_PLAYING);
	gst_element_set_state(c->audpipe, GST_STATE_PLAYING);
}


static void
clienthandler(void *arg)
{
	Clientinfo *c = (Clientinfo*)arg;
	int fd = c->fd;
	char buffer[256];
	char *s;
	char res[1024];
	int i, j, port, foundw;


	printf("started a new handler\n");

	for (i = 0; i < 4; i++) {
		c->watchers[i].port = 0;
	}

	while(1) {
		//memset(res, 0, 1024*sizeof(res));
		if (Readline(fd, buffer, 255) == 0)
			goto done;

		/* Read the first token */
		//printf("read %s", buffer);
		s = strtok(buffer, " ");
		if (!strncmp(s, "login", 5)) {
			s = strtok(NULL, " ");
			if (!strncmp(s, "wearable", 8)) {
				c->iswearable = 1;
				s = strtok(NULL, "\n");
				printf("got username %s\n", s);
				c->name = strdup(s);
				c->vidport = BASEPORT + 2 * (c->index);
				c->audport = c->vidport + 1;
				initgst(c);		// Set up the Gstreamer stuff
				sprintf(res, "hello %d\n", c->vidport);
				for (i = 0; i < MAXCLIENTS; i++) {
					if (i != c->index && clients[i]->name != NULL) {
						if (!strcmp(s, clients[i]->name)) {
							sprintf(res, "hello error\n");
						}
					}
				}
				write(fd, res, strlen(res));
			} else if (!strncmp(s, "server", 6)) {
				c->iswearable = 0;
				write(fd, "hello\n", sizeof("hello\n"));
			} else {
				printf("wtf error\n");
			}
		} else if (!strncmp(s, 	"getlist", 7)) {	
			strcpy(res, "list ");
			for (i = 0; i < MAXCLIENTS; i++) {
				if (clients[i]->iswearable == 1 && clients[i]->name != NULL) {
					strcat(res, clients[i]->name);
					strcat(res, ",");
				}
			}
			strcat(res, "\n");
			write(fd, res, strlen(res));
		} else if (!strncmp(s, "getstream", 9)) {
			foundw = 0;
			s = strtok(NULL, " ");
			port = atoi(s);
			s = strtok(NULL, "\n");
			printf("user on %s requested %s\n", c->ip, s);
			for (i = 0; i < MAXCLIENTS; i++) {
				if (clients[i]->iswearable == 1 && clients[i]->ip != NULL) {
					printf("checking against %s\n", clients[i]->name);
					if (!strcmp(s, clients[i]->name)) {
						printf("found a match\n");
						for (j = 0; j < 4; j++) {
							if (clients[i]->watchers[j].port == 0) {
								newsink(i, j, c->index, port, c->ip);
								write(fd, "stream\n", 7);
								foundw = 1;
								goto itsover;
							}
						}
					}
				}
			}
itsover:
			if (!foundw)
				write(fd, "stream full\n", sizeof("stream full\n"));
		} else if (!strncmp(s, "closestream", 11)) {
			foundw = 0;
			s = strtok(NULL, " ");
			port = atoi(s);
			for (i = 0; i < MAXCLIENTS; i++) {
				if (clients[i]->ip != NULL) {
					for (j = 0; j < 4; j++) {
						if(clients[i]->watchers[j].clientindex == c->index && clients[i]->watchers[j].port == port) {
							closesink(i, j);
							write(fd, "closed\n", 7);
							foundw = 1;
						}
					}
				}
			}
			if (!foundw)
				write(fd, "close failed\n", sizeof("close failed\n"));
		} else if (!strncmp(s, "done", 4)) {
			write(fd, "done\n", 5);
			goto done;
		} else if (!strncmp(s, "getviewers", 10)) {
			s = strtok(NULL, "\n");
			strcpy(res, "viewers ");
			for (i = 0; i < MAXCLIENTS; i++) {
				if (clients[i]->iswearable) {
					if (!strcmp(s, clients[i]->name)) {
						for (j = 0; j < 4; j++) {
							if (clients[clients[i]->watchers[j].clientindex]->iswearable) {
								if (!strcmp(s, clients[clients[i]->watchers[j].clientindex]->name)) {
								} else if (clients[i]->watchers[j].port != 0) {
									strcat(res, clients[clients[i]->watchers[j].clientindex]->name);
									strcat(res, ",");
								}
							}
						}
						break;
					}
				}
			}
			strcat(res, "\n");
			write(fd, res, strlen(res));
		}
	}

done:
	printf("exiting\n");
	cleanupclient(c);
	printf("cleaned client, closing fd\n");
	close(fd);
	printf("closed fd, exiting pthread\n");
	pthread_exit(NULL);
	printf("wtf mate\n");

}

void
sigcatch(int blah)
{
	int i;
	printf("caught interrupt, dying\n");
	close(serversock);
	for (i = 0; i < MAXCLIENTS; i++) {
		close(clients[i]->fd);
		pthread_cancel(threads[i]);
	}
	pthread_exit(0);
}

int
main(int argc, char *argv[])
{
	int on;
	int addr_size, fd, i;
	struct sockaddr_in servaddr;
	char *input;
	struct sockaddr_in clientaddr;

	gst_init(NULL, NULL);

	// initialize the array
	for (i = 0; i < MAXCLIENTS; i++) {
		clients[i] = malloc(sizeof(Clientinfo));
		memset(clients[i], 0, sizeof(Clientinfo));
	}

	signal(SIGINT, (void*)sigcatch);

	serversock = socket(AF_INET, SOCK_STREAM, 0);
	on = 1;
	setsockopt(serversock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(59000);

//	getaddrinfo(NULL, "59000", &hints, &res);

	bind(serversock, (struct sockaddr *)&servaddr, sizeof(servaddr));

	listen(serversock, 10);

	printf("listening...\n");
	while (1) {
		unsigned int clientlen = sizeof(clientaddr);
		fd = accept(serversock, (struct sockaddr *)&clientaddr, &clientlen);
		for (i = 0; i < MAXCLIENTS; i++) {
			if (clients[i]->ip == NULL) { 
				clients[i]->index = i;
				clients[i]->fd = fd;
				printf("Got client #%d, I think the ip address is %s\n", i, inet_ntoa(clientaddr.sin_addr));
				clients[i]->ip = strdup(inet_ntoa(clientaddr.sin_addr));
				printf("got a connection from %s\n", clients[i]->ip);
				pthread_create(&threads[i], NULL, clienthandler, clients[i]);
				break;
			}
		}
	}
}

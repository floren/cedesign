#include "framegrabber.h"
#include "networking.h"

gboolean my_bus_callback(GstBus * bus, GstMessage *message, gpointer data)
{
	g_print ("\nSource: %s  ", GST_MESSAGE_SRC_NAME(message)); // Print calling element
	g_print ("Got %s message	", GST_MESSAGE_TYPE_NAME (message)); // Type of message
	switch (GST_MESSAGE_TYPE (message)) { // Switch on message type
		case GST_MESSAGE_STATE_CHANGED: // Print full state change information
			GstState oldstate;
			GstState newstate;
			GstState pending;
			gst_message_parse_state_changed(message, &oldstate,&newstate,&pending);
			if (oldstate != NULL) 
			{
				g_print("Old State: %d ",oldstate);
				g_print("New State: %d ",newstate);
				g_print("Pending State: %d\n",pending);
			}
			break;

		case GST_MESSAGE_ERROR: // Pipeline Error occurred.  Print error and kill
			GError *err;	// system
			gchar *debug;

			gst_message_parse_error (message, &err, &debug);
			g_print ("Error: %s\n", err->message);
			g_error_free (err);
			g_free (debug);

			g_main_loop_quit (loop);
			break;

		case GST_MESSAGE_EOS:
			/* end-of-stream */
			g_print ("End of Stream Message\n");
			break;
		default:
			/* unhandled message */
			break;

	}
	return TRUE;

}

void streamerInit()
{
	// Videofilter
	GstCaps *caps;
	// Outgoing Pipelines
	videoPipeline = gst_pipeline_new("videoPipeline");
	audioPipeline = gst_pipeline_new("audioPipeline");
	/* create video elements */
//	bus = gst_pipeline_get_bus(GST_PIPELINE(videoPipeline));
//	gst_bus_add_watch(bus, my_bus_callback, NULL);
//	gst_object_unref(bus);
	src = gst_element_factory_make("v4l2src", "video-src"); // Webcam Source
	splitter = gst_element_factory_make("tee", "video-splitter"); // Element to split view to show locally
	sink = gst_element_factory_make("xvimagesink", "video-localsink"); // Local Output Sink
	videoStream = gst_element_factory_make("udpsink", "video-transmitter"); // Transmitter for video (over UDP)
	queue1 = gst_element_factory_make("queue","queue1"); // Queue elements needed for Tee element split
	queue2 = gst_element_factory_make("queue","queue2");

	videoDec = gst_element_factory_make("jpegdec", "decoder"); // Decoder for local streams
	videoFilter = gst_element_factory_make ("ffmpegcolorspace", "filter"); // Colorspace filter for local streams

	audioSrc = gst_element_factory_make("autoaudiosrc","audioSrc"); // Automatically pulls audio source (microphone)
//	audioEnc = gst_element_factory_make("vorbisenc","audioEnc");	// Encode audio (Ogg Vorbis, open-source)
//	audioConvert = gst_element_factory_make("audioconvert", "audioConvert"); // Convert to a proper range
	audioStream = gst_element_factory_make("udpsink","audio-transmitter"); // Transmitter for audio (over UDP)



	if (!videoPipeline || !audioPipeline) // Check Pipeline Creation
	{
		fprintf(stderr, "GStreamer Error: Could not create pipeline\n");
	}
	if(!src || !videoStream || !sink || !videoDec || !videoFilter) // Check Local Video Element Creation
	{

		fprintf(stderr, "GStreamer Error: Video Elements: Could not create element\n");
	}

	if (!audioSrc || !audioStream)  // Check Local Audio Element Creation
	//if (!audioSrc || !audioEnc || !audioStream || !audioConvert) // Check Local Audio Element Creation
	{

		fprintf(stderr, "GStreamer Error: Audio Elements: Could not create element\n");
	}	
	if (!splitter) // Check Tee element creation
	{

		fprintf(stderr, "GStreamer Error: Splitter Element: Could not create video splitter\n");
	}

	if (!videoStream || !audioStream) // Check UDP Transmitter creation
	{
		printf("Stream Creation failure.\n");
	}

	g_object_set(G_OBJECT(src),"device","/dev/video1",NULL);	// Set video input to webcam 
									// (video1 due to local webcam on netbook)

	// Set pad for video transmission (for proper viewing)
	caps = gst_caps_new_simple ("image/jpeg",
			"width", G_TYPE_INT, 320,
			"height", G_TYPE_INT, 240,
			"framerate",GST_TYPE_FRACTION,100,1,
			NULL);

	audCaps1 =  gst_caps_new_simple("audio/x-raw-int",
			"rate", G_TYPE_INT, 8000,
			"channels", G_TYPE_INT,2,
			"endianness", G_TYPE_INT, 1234,
			"signed", G_TYPE_BOOLEAN, FALSE,
			"width", G_TYPE_INT, 8,
			"depth", G_TYPE_INT, 8,
			NULL);

	// UDP Connection Setting (taken from variable ipaddr in networking.h)
	g_object_set(G_OBJECT(videoStream),"host",ipaddr, "port", outputPort, NULL);
	g_object_set(G_OBJECT(audioStream),"host",ipaddr, "port", outputPort+1, NULL);

	// Add Video Pipeline elements to proper bin
	gst_bin_add_many(GST_BIN(videoPipeline),src, splitter, sink, videoDec, videoFilter, videoStream, queue1, queue2, NULL);

	// Add Audio Pipeline elements to proper bin
	//gst_bin_add_many(GST_BIN(audioPipeline),audioSrc,audioConvert, audioEnc,audioStream, NULL);
	gst_bin_add_many(GST_BIN(audioPipeline),audioSrc,audioStream, NULL);

	if(!gst_element_link_many(src, splitter, NULL)) // Link Tee element
	{
		printf("Source -> Splitter  link failure\n");
	}

	if(!gst_element_link_filtered(audioSrc, audioStream, audCaps1)) // Link full audio pipeline
	//if(!gst_element_link_many(audioSrc, audioConvert, audioEnc,audioStream, NULL)) // Link full audio pipeline
	{
		printf("Audio link failure\n");
	}

	pad1 = gst_element_get_request_pad(splitter, "src%d"); // Create Tee element pads 
	pad2 = gst_element_get_request_pad(splitter, "src%d"); // Two pads, one for transmission, the other for local viewing

	gst_element_link(splitter, queue1); // Link elements to queues (need to be linked to queues to properly 
	gst_element_link(splitter, queue2); // stream local data

	if(!gst_element_link_filtered (queue1, videoStream, caps)) { // Setup outgoing video with given pad settings (from caps)
		printf("Link between queue and stream failed.\n");
	}

	if (!gst_element_link_many(queue2, videoDec, videoFilter, sink, NULL)) // Setup local video viewer
	{
		printf("queue -> Local Src link failed\n");
	}



	// Incoming Video
	incVidPipeline = gst_pipeline_new("incomingVideoPipeline"); // Pipeline init
	incVidSrc = gst_element_factory_make("udpsrc","incomingVideoUDP"); // Incoming UDP Reciever
	incVidDec = gst_element_factory_make("jpegdec","incomingVideoDecoder"); // Data -> jpeg Decoder
	incVidFilter = gst_element_factory_make("ffmpegcolorspace","incomingColorFilter"); // Colorspace filter
	incVidSink = gst_element_factory_make("xvimagesink", "incomingVideoSink"); // Sink for viewing recieved images

	if (!incVidPipeline) // Check pipeline creation
	{
		fprintf(stderr, "Gstreamer Error: Could not create incoming video pipeline\n");
	}

	if (!incVidSrc || !incVidDec || !incVidFilter) // Check incoming video pipeline elements
	{
		fprintf(stderr, "Gstreamer Error: Could not create incoming video elements\n");
	}

	// Incoming Audio
	incAudPipeline = gst_pipeline_new("incomingAudioPipeline"); // Pipeline init
	incAudSrc = gst_element_factory_make("udpsrc","incomingAudioUDP"); // Incoming UDP Reciever
//	incAudDec = gst_element_factory_make("vorbisdec", "incomingAudioDecoder"); // Data -> Ogg Vorbis Audio decoder
//	incAudConvert = gst_element_factory_make("audioconvert", "incomingAudioConvert"); // Convert to proper range 
	incAudSink = gst_element_factory_make("alsasink", "incomingAudioSink"); // Play over alsa interface

	audCaps2 =  gst_caps_new_simple("audio/x-raw-int",
			"rate", G_TYPE_INT, 8000,
			"channels", G_TYPE_INT,2,
			"endianness", G_TYPE_INT, 1234,
			"signed", G_TYPE_BOOLEAN, FALSE,
			"width", G_TYPE_INT, 8,
			"depth", G_TYPE_INT, 8,
			NULL);


	if (!incAudPipeline) // Check Audio Pipeline Creation
	{
		fprintf(stderr, "Gstreamer Error: Could not create incoming audio pipeline\n");
	}


	if (!incAudSrc || !incAudSink ) // Check Audio Element Creation
//`	if (!incAudSrc || !incAudSink || !incAudDec || !incAudConvert) // Check Audio Element Creation
	{
		fprintf(stderr, "Gstreamer Error: Could not create incoming audio elements\n");
	}

	// Add incoming audio and video elements to proper bins	
	gst_bin_add_many(GST_BIN(incVidPipeline), incVidSrc, incVidDec, incVidFilter, incVidSink, NULL);
	gst_bin_add_many(GST_BIN(incAudPipeline), incAudSrc, incAudSink, NULL);
	//gst_bin_add_many(GST_BIN(incAudPipeline), incAudSrc, incAudDec, incAudConvert, incAudSink, NULL);

	if (!gst_element_link_many(incVidSrc,incVidDec, incVidFilter, incVidSink, NULL)) // Link incoming video elements
	{
		fprintf(stderr, "Gstreamer Error: Incoming Video Element Linkage Failure");
	}

	if (!gst_element_link_filtered(incAudSrc, incAudSink, audCaps2)) // Link incoming audio elements
	//if (!gst_element_link_many(incAudSrc, incAudDec, incAudConvert, incAudSink, NULL)) // Link incoming audio elements
	{
		fprintf(stderr, "Gstreamer Error: Incoming Audio Element Linkage Failure");
	}

	// All incoming video and audio will come at ports 60000 and 60001 respectively
	g_object_set(G_OBJECT(incVidSrc), "port", 60000, NULL);
	g_object_set(G_OBJECT(incAudSrc), "port", 60001, NULL);

	g_object_set(G_OBJECT(incAudSink), "sync", FALSE, NULL);
	// Set outgoing pipelines to play
	gst_element_set_state(videoPipeline, GST_STATE_PLAYING);
	gst_element_set_state(audioPipeline, GST_STATE_PLAYING);

	// Dereference objects no longer needed
	gst_object_unref(GST_OBJECT(pad1));
	gst_object_unref(GST_OBJECT(pad2));
	gst_caps_unref (caps);
	gst_caps_unref (audCaps1);
	gst_caps_unref (audCaps2);
}

void setIncomingStream()
{
	// Make sure elements are not removed twice
	if (initViewClear == 0)
	{
		// Set outgoing/local video pipeline to initial state
		gst_element_set_state(videoPipeline, GST_STATE_NULL);
		gst_element_set_state(audioPipeline, GST_STATE_NULL);

		// Remove tee (splitter) from element flow
		gst_element_unlink(src, splitter);
		gst_element_unlink(splitter, queue1);

		// Relink src to first queue
		if (!gst_element_link(src,queue1))
		{
			fprintf(stderr, "Src -> Queue1 link failed\n");
		}

		// Remove all local viewing (not incoming) elements from pipeline
		gst_bin_remove_many(GST_BIN(videoPipeline), videoDec, videoFilter, sink, splitter, queue2, NULL);

		// Set outgoing pipeline into play state
		gst_element_set_state(videoPipeline, GST_STATE_PLAYING);
		gst_element_set_state(audioPipeline, GST_STATE_PLAYING);

		// Set incoming pipeline into play state
		gst_element_set_state(incVidPipeline, GST_STATE_PLAYING);
		gst_element_set_state(incAudPipeline, GST_STATE_PLAYING);

		// Ensure that once this is called, it is not called again 
		// (to prevent double dereferencing)
		initViewClear = 1;
	}
}


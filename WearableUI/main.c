/***************************************************
 *
 * main.c 
 *
 * Project: CE Senior Design
 *
 * Description: Main Program Driver for Wearable UI
 *
 * Authors: Mark Stump
 * 	    Daniel Liu
 * 	    John Floren
 *
 ****************************************************/


#include <string.h>
#include "Global.h"
#include "gui.h"
#include "framegrabber.h"
#include "networking.h"

// Set input file
FILE *config;

int main(int argc, char *argv[])
{
	// Line reading buffer
	char line[80];

	// File reading variables
	char *username;
	char *host;
	char *tokened;

	char *homeDir;	
	// Open file "config.conf" in read mode
	homeDir = getenv("HOME");
	sprintf(homeDir, "%s/cedesign/WearableUI/config.conf", homeDir);
	config = fopen(homeDir, "rt"); // Open configuration file for reading

	// While there is data in the file
	while (fgets(line, 80, config) != NULL)
	{
		// Tokenize string
		tokened = strtok(line," ");
		if (strcmp(tokened,"username") == 0) // If username....
		{
			username = strtok(NULL," ");
			setUsername(username); // Set username in networking interface
		}
		else if (strcmp(tokened, "server_ip") == 0) // If server ip....
		{
			int len;
			host = strtok(NULL, " ");
			len = strlen(host);
			if (host[len-1] == '\n') // Since last piece of data in file, remove newline
			{
				host[len-1] = 0;
			}
			setServer(host); // Set server IP in netwokring interface
		}
	}

	initThreads(); // Initialize threading system
	g_thread_create(main_server_thread, NULL, FALSE, NULL);	// Create server communication thread
	loop = g_main_loop_new(NULL,FALSE); // Setup GTK GUI thread

	streamerInit(); // Initialize Streamer System
	init_gui(argc, argv); // Intitialize GUI
	
	// Hand control over to the GTK Framework
	g_main_loop_run(loop);

	return 0;
}

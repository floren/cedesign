/*************************************************
 * gui.h
 *
 * Project: CE Senior Design Project
 *
 * Description: GUI header file for initialization
 *		and running of Wearable GUI
 *
 *      Authors: Mark Stump
 *      	 Daniel Liu
 *      	 John Floren
 *************************************************/

#ifndef GUI_H_
#define GUI_H_

#include "Global.h"
#include <unistd.h>
#include <pthread.h>
#include <gtk/gtk.h>
#include "framegrabber.h"

char *meViewing;

// Initialization of GUI call
void init_gui(int argc, char *argv[]);

// Method for updating the GUI Clock
gboolean clockUpdate();

// Event called on closing of GUI
gboolean delete_event (GtkWidget *widget,
		GdkEvent *event, gpointer data);

// Show widgets (userlist, clock, transmitting status, etc)
void display_menu_widgets();

// Hide widgets
void hide_menu_widgets();

// Handler for chord presses
gboolean key_press_handler(GtkWidget *widget,
		GdkEventKey *event, gpointer data);


//void init_list(GtkWidget *list);

//void clear_list();

// Update list with people currently viewing the user
gboolean setViewingUsers();

// Update list of users currently using the system
gboolean userListUpdate();

//void set_list();
//void add_to_list(GtkWidget *list, const gchar *s);

// Initialize GStreamer and GTK Threads
void initThreads();

// Method used for displaying video 
gboolean expose_cb (GtkWidget *widget, GdkEventExpose *event, gpointer data);


#endif /* GUI_H_ */

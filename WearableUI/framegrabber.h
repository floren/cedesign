/*
 *	framegrabber.h
 *
 *	Project: CE Senior Design 
 *
 *      Author: Mark Stump
 *
 *      Description:
 *      Header file for the management of video and audio streams
 */

#ifndef FRAMEGRABBER_H_
#define FRAMEGRABBER_H_

#include <gtk/gtk.h>
#include <gst/gst.h>
#include <glib.h>
#include "Global.h"


// Serves as a message bus to view states of pipeline
gboolean my_bus_callback(GstBus * bus, GstMessage *message, gpointer data);

// Initialize Streams for Unit
void streamerInit();

// Setup streams for incoming data
void setIncomingStream();


#endif /* FRAMEGRABBER_H_ */

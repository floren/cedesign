/********************************************************
 * networking.h
 *
 * Project: CE Senior Design
 *
 * Description: Networking interface header file
 * 
 * Authors: Mark Stump
 * 	    Daniel Liu
 * 	    John Floren
 ********************************************************/

#ifndef NETWORKING_H_
#define NETWORKING_H_
#include <netdb.h>

#include "socket_helper.h"

// Server Commands
const char* login_mesg = "login wearable"; // login wearable [username]
const char* getlist_mesg = "getlist\n";	// Get list of users
const char* getstream_mesg = "getstream 60000"; // getstream 60000 [user name]
const char* closestream_mesg = "closestream 60000\n"; // Close stream currently viewing
const char* done_mesg = "done\n";	// Complete (wearable shutdown)

char login[50]; // Login string buffer
char reqStream[50]; // Stream request buffer

int firstFlag = 0;
int serversock; // Server socket holder
char buf[100]; // Server communication buffer
struct hostent *he; // Host Entity variable
struct sockaddr_in servaddr;

char* ipaddr; // Given IP holder
char  userName[30]; // Username buffer
int outputPort; // Output port from server (used for incoming stream setup)

// Set server to recieve from
void setServer(char* IP);

// Set username of this system
void setUsername(char* name);

// Main server communication driver
void* main_server_thread(void* ptr);

// Request user list from server
char* getUserList();

// Request list of users viewing self 
char* getViewers();

// Request a stream from the server
void requestStream(char* user);

// Shutdown connection to server
void shutdown();

#endif /* NETWORKING_H_ */

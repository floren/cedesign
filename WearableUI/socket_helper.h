/*
 * socket_helper.h
 *
 *  Created on: Oct 24, 2009
 *      Author: daliu
 */

#ifndef SOCKET_HELPER_H_
#define SOCKET_HELPER_H_

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>


int Readline(int sockd, void *vptr, int maxlen);
int Writeline(int sockd, const void *vptr, int n);

#endif /* SOCKET_HELPER_H_ */

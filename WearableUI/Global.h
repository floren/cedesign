/*************************************************
 *
 * Global.h
 *
 * Project: CE Senior Design Project
 *
 * Description:
 * Global variable/Import holding file
 *
 * Authors:	Mark Stump
 * 	 	Daniel Liu
 *		John Floren
 *
 *************************************************/

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gst/interfaces/xoverlay.h>
#include <gst/gst.h>
#include <gtk/gtk.h>



// List counters and flags for navigation/operation
// of UI
int selectedIndex;
int maxIndex;
int transmitting;
int viewCamera;
int viewSelf;

int initViewClear = 0;


// List selection variable for user list
GtkTreeSelection *selection;
// Primary window component declaration
GtkWidget *window;
GtkWidget *userlabel;
GtkWidget *vbox;
GtkWidget *userLabelHolder;
GtkWidget *statusLabelHolder;
GtkWidget *timeLabelHolder;
GtkWidget *statusLabel;
GtkWidget *timeLabel;
GtkWidget *list;
GtkWidget *image;

// GUI Loop Declaration
GMainLoop *loop;

// GStreamer Components

// Outgoing Video
GstElement *videoPipeline;
GstBus *bus;
GstElement *src;
GstElement *sink;
GstElement *videoStream;
GstElement *videoDec;
GstElement *videoFilter;
GstElement *splitter;
GstPad *pad1;	// Pads for Splitter Element
GstPad *pad2;
GstElement *queue1; // Queues for post-pad element
GstElement *queue2;

// Outgoing Audio
GstElement *audioPipeline;
GstElement *audioSrc;
GstElement *audioConvert;
GstElement *audioEnc;
GstElement *audioStream;

// Incoming Video
GstElement *incVidPipeline;
GstElement *incVidSrc;
GstElement *incVidDec;
GstElement *incVidFilter;
GstElement *incVidSink;

// Incoming Audio
GstElement *incAudPipeline;
GstElement *incAudSrc;
GstElement *incAudDec;
GstElement *incAudConvert;
GstElement *incAudSink;

GstCaps *audCaps1;
GstCaps *audCaps2;


#endif 

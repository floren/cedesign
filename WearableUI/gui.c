#include <gdk/gdkx.h>
#include <gst/interfaces/xoverlay.h>

#include "networking.h"
#include "gui.h"

void initThreads()
{
	// Initalize threads for GStreamer and GUI
	gst_init(NULL,NULL);
	gtk_init(NULL, NULL);
}

gboolean clockUpdate() 
{
	// Pull local time from computer and 
	// display to user
	char *date;
	time_t timer;
	timer = time(NULL);
	date = asctime(localtime(&timer));
	gtk_label_set_text(GTK_LABEL(timeLabel),date);
	return TRUE;
}

gboolean userListUpdate()
{
	char *userList; // Pointer for pulling user list data
	char *usertoken; // Variable for tokenizing retrieved user list
	char *viewingtoken; // Variable for tokenizing viewing list
	char *viewingString; // Variable for storing viewing list

	char *usersViewing[50];
	// Create list of viewing users	
	viewingString = getViewers();
	viewingtoken = strtok(viewingString, " ");
	viewingtoken = strtok(NULL, ",");

	int viewCounter = 0;
	//	viewingUsers = NULL;
	while(viewingtoken != NULL)
	{
		if (strcmp(viewingtoken, "\n") != 0) // If pulled token is not a newline character..
		{
			usersViewing[viewCounter] = (char*)malloc(strlen(viewingtoken)+1);
			memcpy(usersViewing[viewCounter], viewingtoken, strlen(viewingtoken)+1);
			//			printf("Added to local array: %s\n", usersViewing[viewCounter]);
			viewingtoken = strtok(NULL, ",");
			viewCounter = viewCounter + 1;
		}
		else
		{
			viewingtoken = NULL;
		}
	}

	//	printf("Post viewing list parsing\n");

	gtk_clist_freeze((GtkCList *)list); // Freeze list to prevent view flickering
	gtk_clist_clear((GtkCList *)list); // Clear old content from list

	// Tokenize string on commas (comma seperated list
	userList = getUserList(); // Retrieve user list from server
	usertoken = strtok(userList, " "); // Begin tokenizing of user list
	usertoken = strtok(NULL, ",");
	maxIndex = 0; // Reset maximum index
	int indx = 0;
	char insertData[100];
	char * listData;
	while(usertoken != NULL)
	{
		if (strcmp(usertoken, "\n") != 0) // If pulled token is not a newline character..
		{
			int listCounter;

			sprintf(insertData, "%s", usertoken);
			for (listCounter = 0; listCounter < viewCounter; listCounter++)
			{
				int ret = strcmp(usertoken, usersViewing[listCounter]);
				//				printf("String Compare Result: %d\n", ret);
				//				printf("Iterated Viewing Array Element: %s\n", usersViewing[listCounter]);
				if (ret == 0)
				{
					sprintf(insertData, "%s (Viewing You)", usertoken);
				}
			}

			if (meViewing != NULL)
			{
				if (strcmp(usertoken, meViewing) == 0)
				{
					sprintf(insertData, "%s (Now Viewing)", insertData);
				}
			}
			listData = strdup(insertData);		

			gtk_clist_append((GtkCList *)list, &listData); // Add to list

			gtk_clist_set_row_data((GtkCList *)list, indx, usertoken); // Set data to pull from list to username

		//	printf("Row Text Inserted: %s\n", listData);
		//	printf("Row Data Inserted: %s\n",usertoken);


			usertoken = strtok(NULL, ","); // Get next token

			indx = indx + 1; // Increment element counter
		}
		else
		{
			usertoken = NULL; // If token is newline, set token to null and move on
		}
	}
	maxIndex = indx; // Set maxindex to number of elements
	if (selectedIndex <maxIndex)
	{
		gtk_clist_select_row((GtkCList *) list, selectedIndex, 0); // Reset chosen value in GUI
	}

	gtk_clist_thaw((GtkCList *)list); // Thaw list to show changes 

	// Cleanup memory
	for (indx = 0; indx <viewCounter; indx++)
	{
		free(usersViewing[indx]);
	}
	return TRUE;
}

void init_gui(int argc, char *argv[])
{
	// Background color declaration
	GdkColor userLabelColor;
	GdkColor statusLabelColor;
	GdkColor timeLabelColor;

	// Initializing selected and maximum index
	selectedIndex = 0;
	maxIndex = -1;
	transmitting = 1;
	viewCamera = 0;

	// Setup colors for backgrounds
	gdk_color_parse("lightblue", &userLabelColor);
	gdk_color_parse("lightgreen", &statusLabelColor);
	gdk_color_parse("red", &timeLabelColor);

	// Instantiate GTK Components
	vbox = gtk_vbox_new(FALSE,0);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	// Informative labels
	userlabel = gtk_label_new("Active Users:");
	statusLabel = gtk_label_new("Transmission Status: ACTIVE");
	timeLabel = gtk_label_new("Waiting for Time Update");

	// Set fonts of labels large for user readability
	gtk_widget_modify_font(GTK_WIDGET(userlabel), pango_font_description_from_string("Tahoma 25"));
	gtk_widget_modify_font(GTK_WIDGET(statusLabel), pango_font_description_from_string("Tahoma 25"));
	gtk_widget_modify_font(GTK_WIDGET(timeLabel), pango_font_description_from_string("Tahoma 25"));
	// List initialization
	list = gtk_clist_new(1);
	gtk_widget_modify_font(GTK_WIDGET(list), pango_font_description_from_string("Tahoma 25"));
	gtk_clist_set_selection_mode(GTK_CLIST(list),GTK_SELECTION_SINGLE);
	gtk_clist_set_shadow_type(GTK_CLIST(list), GTK_SHADOW_NONE);

	// Labels for backgrounds (for background colors)
	userLabelHolder = gtk_event_box_new();
	statusLabelHolder = gtk_event_box_new();
	timeLabelHolder = gtk_event_box_new();

	// Setup primary window parameters
	gtk_window_set_title(GTK_WINDOW(window), "");

	// Event callbacks
	g_signal_connect(G_OBJECT(window),"delete_event", G_CALLBACK(delete_event), NULL);
	g_signal_connect(G_OBJECT(window),"key_press_event", G_CALLBACK(key_press_handler), NULL);
	g_signal_connect(G_OBJECT(window),"expose-event",G_CALLBACK(expose_cb),sink);

	// Set container to small
	gtk_container_set_border_width(GTK_CONTAINER(window),0);
	// Set size of window to VGA Resolution (QVGA unsupported by converter box)
	gtk_widget_set_size_request(window,640,480);

	// Container setup (text labels put inside event boxes
	// to use background colors)
	// Pack first item into box
	gtk_container_add(GTK_CONTAINER(userLabelHolder),userlabel); // Active Users Label
	gtk_box_pack_start(GTK_BOX(vbox), userLabelHolder, FALSE, FALSE, 5);

	// Place user list after first label
	gtk_box_pack_start(GTK_BOX(vbox), list, FALSE, FALSE, 5);

	// Add other labels to bottom of box
	gtk_container_add(GTK_CONTAINER(timeLabelHolder),timeLabel); // Current Time Label
	gtk_box_pack_end(GTK_BOX(vbox), timeLabelHolder, FALSE, FALSE, 5);

	gtk_container_add(GTK_CONTAINER(statusLabelHolder),statusLabel); // Transmission Status Label
	gtk_box_pack_end(GTK_BOX(vbox), statusLabelHolder, FALSE, FALSE, 5);

	// Setup user list
	gtk_clist_set_column_justification(GTK_CLIST(list),0,GTK_JUSTIFY_CENTER);
	gtk_clist_set_column_width(GTK_CLIST(list),0,320);

	// Set label background colors
	gtk_widget_modify_bg(GTK_WIDGET(userLabelHolder),(GtkStateType)0,&userLabelColor);
	gtk_widget_modify_bg(GTK_WIDGET(statusLabelHolder),(GtkStateType)0,&statusLabelColor);
	gtk_widget_modify_bg(GTK_WIDGET(timeLabelHolder),(GtkStateType)0,&timeLabelColor);

	// Final add, adding the verticle box to the primary window container
	gtk_container_add(GTK_CONTAINER(window), vbox);

	// Pack box into GUI
	gtk_box_pack_end(GTK_BOX(vbox), image, FALSE,FALSE,5);

	// Set timeouts for GUI Updates
	g_timeout_add(1000, (GtkFunction)clockUpdate,NULL); // Update clock every 1000 ms (1 s)

	g_timeout_add(1500, (GtkFunction)userListUpdate, NULL); // Update user list every 1000 ms (1 s)
	// Viewing self by default
	viewSelf = 1;
	// Show all applicable widgets
	display_menu_widgets();
}



// Event handler for expose event
gboolean expose_cb (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
	// Show both streams (only one will be transmitting at a time)
	gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(sink), GDK_WINDOW_XWINDOW(widget->window));
	gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(incVidSink), GDK_WINDOW_XWINDOW(widget->window));
	return TRUE;	
}	

// Event handler on the main window to handle the close button press
gboolean delete_event (GtkWidget *widget, GdkEvent *event, gpointer data)
{
	// Send command to server to shutdown streaming
	shutdown();
	// End control by GTK, dispose of window with FALSE comand
	gtk_main_quit();
	exit(0);
	return FALSE;
}
void display_menu_widgets()
{

	// Show all applicable widgets
	gtk_widget_show(userLabelHolder);
	gtk_widget_show(userlabel);
	gtk_widget_show(statusLabelHolder);
	gtk_widget_show(statusLabel);
	gtk_widget_show(timeLabelHolder);
	gtk_widget_show(timeLabel);
	gtk_widget_show(list);
	gtk_widget_show(vbox);
	gtk_widget_show(window);
	viewCamera = 1;
}

void hide_menu_widgets()
{

	// Hide all applicable widgets
	gtk_widget_hide(userLabelHolder);
	gtk_widget_hide(userlabel);
	gtk_widget_hide(statusLabelHolder);
	gtk_widget_hide(statusLabel);
	gtk_widget_hide(timeLabelHolder);
	gtk_widget_hide(timeLabel);
	gtk_widget_hide(list);
	viewCamera = 0;
}

// Event handler on main window to handle user keyboard input
gboolean key_press_handler(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	switch(event->keyval)
	{
		case 32:
			// Select current feed
			// Spacebar keypress
			printf("Spacebar Chord Pressed\n");
			char* text;
			text =strdup((char *) gtk_clist_get_row_data((GtkCList*) list, selectedIndex)); // Get username from list

			printf("Text found (before request) (username): %s\n",text);

			meViewing = strdup(text);
			requestStream(text); // Request stream based on selected user
			setIncomingStream(); // Setup system for accepting streams

			printf("Text found (username): %s\n",text);
			//			sprintf(meViewing, "%s", text);	
			viewSelf = 0; // No longer viewing local feed
			break;
		case 105:
			// Select previous user
			// 'i' keypress
			printf("i Chord Pressed\n");
			selectedIndex = selectedIndex-1;
			if (selectedIndex < 0) // Prevent from going below 0
			{
				selectedIndex = 0;
			}
			gtk_clist_select_row((GtkCList*)list, selectedIndex, 0); // Set selection row
			break;
		case 114:
			// Select next user
			// 'r' keypress
			selectedIndex = selectedIndex + 1;

			// -1 used with maxIndex due to zero-based counting of 
			// information
			if (selectedIndex > (maxIndex-1))
			{
				selectedIndex = (maxIndex-1);
			}
			printf("r Chord Pressed\n");
			gtk_clist_select_row((GtkCList*)list, selectedIndex, 0); // Set Selection row
			break;
		case 121:
			// Hide/Show menu  options
			// 'y' keypress
			printf("y Chord Pressed\n");
			if (viewCamera ==1)
			{
				hide_menu_widgets(); // Hide widgets (to only show camera feed)
				printf("Hiding Menu Widgets\n");
			}
			else
			{
				display_menu_widgets(); // Show widgets (to show menu options)
				printf("Displaying Menu Widgets\n");
			}
			break;
		case 119:
			// Streaming output chord
			// Trusn streaming on and off

			// 'w' keypress
			printf("w Chord Pressed\n");

			if (transmitting) // If currently transmitting
			{
				// Pause streaming
				gtk_label_set_text(GTK_LABEL(statusLabel),"Transmission Status: INACTIVE"); // Set transmission label
				transmitting = 0; // Clear transmitting flag
				gst_element_set_state(videoPipeline, GST_STATE_PAUSED); // Pause audio and video pipelines
				gst_element_set_state(audioPipeline, GST_STATE_PAUSED);
				printf("Transmission Inactive\n");

			}
			else // If not currently streaming
			{
				// Enable Streaming
				gtk_label_set_text(GTK_LABEL(statusLabel),"Transmission Status: ACTIVE"); // Set transmission label
				transmitting = 1; // Set transmitting flag
				gst_element_set_state(videoPipeline, GST_STATE_PLAYING); // Set video and audio pipelines to playing
				gst_element_set_state(audioPipeline, GST_STATE_PLAYING);
				printf("Transmission Active\n");
			}
			break;
		case 106:
			// Soft Reset Chord
			// 'j' keypress
			printf("j Chord Pressed\n");
			delete_event (NULL, NULL, NULL);
			break;
		default: printf("Invalid Chord Pressed\n");
	}
	return FALSE;
}

/*********************************************
 * networking.c
 *
 * Project: CE Senior Design
 *
 * Description: 
 * Network interface code for communicating
 * with main server for wearable systems
 *
 * Authors: Mark Stump
 * 	    Daniel Liu
 * 	    John Floren
 *********************************************/

#include "networking.h"

void setServer(char* IP)
{
	ipaddr = IP; // Set given server to IP to connect too
}

void setUsername(char* name)
{
	strcpy(userName, name); // Set given username to username for this system
}
void* main_server_thread(void* ptr) {

	// Declare Server Socket
	serversock = socket(AF_INET, SOCK_STREAM, 0);


	// If hostname is not found, error out
	if ((he = gethostbyname(ipaddr)) == NULL) {
		printf("Error resolving hostname.\n");
		exit(EXIT_FAILURE);
	}


	// Clear servaddr memory
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET; // Set type of connection
	memcpy(&servaddr.sin_addr, he->h_addr_list[0], he->h_length);
	servaddr.sin_port = htons(59000); // Set port to connect to server on

	// Connect to server.  If connection fails, error out
	if(connect(serversock, (struct sockaddr *)&servaddr, sizeof(servaddr))) {
		printf("Server Connection Failed!\n");
		exit(EXIT_FAILURE);
	}

	// Create login message with given username
	sprintf(login,"login wearable %s", userName);

	// Write login message to server
	Writeline(serversock, login, strlen(login));

	// Wait for response (to confirm connection and get port to connect too)
	Readline(serversock, buf, 99);

	// Get port to connect to for streaming
	sscanf(buf, "hello %d\n", &outputPort);

	return 0;
}

void requestStream(char* user)
{
	if (user != NULL)
	{
		// Write closestream message to close any outstanding streams
//		if (firstFlag != 0)
//		{
			Writeline(serversock, closestream_mesg, strlen(closestream_mesg));
			Readline(serversock, buf, 99);
			memset(buf,0,99);
//		}
		firstFlag = 1;

		// Setup request stream with given username
		//printf("User name sent: %s\n", user);
		sprintf(reqStream, "getstream 60000 %s\n",user);

		//printf("Command sent: %s\n", reqStream);
		// Write request string to server
		Writeline(serversock, reqStream, strlen(reqStream));

		// Wait for response to confirm connection
		Readline(serversock, buf, 99);
	}
}

char* getUserList()
{
	// Clear memory buffer
	memset(buf, 0,99);
	// Write get user list mesage to server
	
	printf("Command to server: %s\n", getlist_mesg);
	Writeline(serversock, getlist_mesg, strlen(getlist_mesg));
	// Read returned information from server
	Readline(serversock, buf, 99);
	// Return buffer of usernames
	printf("Returned from server: %s\n", buf);
	return buf;
}
char* getViewers()
{
	sprintf(reqStream, "getviewers %s",userName);
	//sprintf(reqStream, "getviewers %s\n",userName);
	// Clear memory buffer
	memset(buf, 0,99);
	// Write get viewing user list to server
	printf("Command to server: %s\n", reqStream);
	Writeline(serversock, reqStream, strlen(reqStream));
	// Read returned information from server
	Readline(serversock, buf, 99);
	printf("Returned from server: %s\n", buf);
	return buf;
}

void shutdown()
{
//	if (firstFlag != 0) // Closing a stream if one is open
//	{
		printf("Command to server: %s\n", closestream_mesg);
		Writeline(serversock, closestream_mesg, strlen(closestream_mesg));
		Readline(serversock, buf, 99);
		printf("Returned from server: %s\n", buf);
		memset(buf,0,99);
//	}

	printf("Command to server: %s\n", done_mesg);
	Writeline(serversock, done_mesg, strlen(done_mesg));
	Readline(serversock, buf, 99);
	printf("Returned from server: %s\n",buf);
}

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <gtk/gtk.h>
#include <string.h>

#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gst/interfaces/xoverlay.h>
#include <gst/gst.h>
#include <gtk/gtk.h>



#define NFEEDS 4        /* the number of feed windows in the program */
enum
{
	NAME = 0,
	N_COLUMNS
};

typedef struct User {
	char* name;
	int isopen;
} User;

int nusers = 3;
User users[3];

int feeds[NFEEDS] = {-1, -1, -1, -1};

GMainLoop *loop;

// GStreamer Components
GstElement *videoPipeline;
GstElement *videoSrc;
GstElement *videoDec;
GstElement *videoFilter;
GstElement *sink1;

GstElement *videoPipeline2;
GstElement *videoSrc2;
GstElement *videoDec2;
GstElement *videoFilter2;
GstElement *sink2;

GstElement *videoPipeline3;
GstElement *videoSrc3;
GstElement *videoDec3;
GstElement *videoFilter3;
GstElement *sink3;

GstElement *audioPipeline;
GstElement *audioSrc;
GstElement *audioDec1;
GstElement *audioConvert1;
GstElement *audioSink1;



// GUI Components
GtkWidget *list;
GtkWidget *table;
GtkTreeSelection *selection;
GtkWidget *blankimage;
GtkWidget *window;
GtkWidget *window2;
GtkWidget *hbox;
GtkWidget *vbox;
GtkWidget *userlabel;
GtkWidget *vids[4];

#endif

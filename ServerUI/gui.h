#ifndef GUI_H_
#define GUI_H_

#include "Global.h"
#include <unistd.h>
#include <pthread.h>
#include <gtk/gtk.h>


void init_gui(int argc, char*argv[]);

gboolean delete_event (GtkWidget *widget, GdkEventKey *event, gpointer data);

void init_list(GtkWidget *list);

void add_to_list(GtkWidget *list, const gchar *s);

void initThreads();

void openuser(gchar *name);

void on_doubleclick(GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *col, gpointer data);

#endif


#include "Global.h"
#include "gui.h"
#include "networking.h"
#include "media.h"

int main(int argc, char *argv[])
{
	loop = g_main_loop_new(NULL, FALSE);

	initThreads();
	
	initStreams();	
	init_gui(argc, argv);

	//g_thread_init(NULL);
	//gdk_threads_init();

	//g_thread_create(main_server_thread, NULL, FALSE, NULL);

	g_main_loop_run(loop);
	return 0;
}

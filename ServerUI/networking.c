/*
 * networking.c
 *
 *  Created on: Oct 24, 2009
 *      Author: daliu
 */

#include "networking.h"


void* main_server_thread(void* ptr) {
	int serversock;
	char buf[100];
	struct hostent     *he;

	struct sockaddr_in servaddr;

	serversock = socket(AF_INET, SOCK_STREAM, 0);


	  if ((he = gethostbyname("129.21.27.223")) == NULL) {
	    printf("Error resolving hostname.\n");
	    exit(EXIT_FAILURE);
	  }



	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	memcpy(&servaddr.sin_addr, he->h_addr_list[0], he->h_length);
	//servaddr.sin_addr.s_addr = htonl();
	servaddr.sin_port = htons(59000);

//	getaddrinfo(NULL, "59000", &hints, &res);

	//bind(serversock, (struct sockaddr *)&servaddr, sizeof(servaddr));

	//listen(serversock, 10);

	if(!connect(serversock, (struct sockaddr *)&servaddr, sizeof(servaddr))) {
		printf("Server Connection Failed!\n");
		exit(EXIT_FAILURE);
	}

	Writeline(serversock, login_mesg, strlen(login_mesg));

	Readline(serversock, buf, 99);
	printf("FROM SERVER: %s\n",buf);

	Writeline(serversock, getlist_mesg, strlen(getlist_mesg));

	Readline(serversock, buf, 99);
	printf("FROM SERVER: %s\n",buf);

	return 0;
}


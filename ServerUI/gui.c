#include "gui.h"
#include <gdk/gdkx.h>
#include <gst/interfaces/xoverlay.h>

void initThreads()
{
	gst_init(NULL, NULL);
	gtk_init(NULL, NULL);
}


void on_doubleclick(GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *col, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *name;

	model = gtk_tree_view_get_model(view);

	if (gtk_tree_model_get_iter(model, &iter, path)) {
		gtk_tree_model_get(model, &iter, NAME, &name, -1);
		g_print ("Double-clicked row contains name %s\n", name);
		openuser(name);
		g_free(name);
	}
}


gboolean delete_event( GtkWidget *widget, GdkEvent *event, gpointer data )
{
	gtk_main_quit ();
	return FALSE;
}
/*

   void add_to_list(GtkWidget *list, const gchar *s)
   {
   GtkListStore *store;
   GtkTreeIter iter;

   store = GTK_LIST_STORE(gtk_tree_view_get_model
   (GTK_TREE_VIEW(list)));

   gtk_list_store_append(store, &iter);
   gtk_list_store_set(store, &iter, NAME, s, -1);
   }

   void init_list(GtkWidget *list)
   {
   GtkCellRenderer *renderer;
   GtkTreeViewColumn *column;
   GtkListStore *store;

   renderer = gtk_cell_renderer_text_new();
   column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME, NULL);
   gtk_tree_view_append_column(GTK_TREE_VIEW (list), column);

   store = gtk_list_store_new(N_COLUMNS, G_TYPE_STRING);

   gtk_tree_view_set_model(GTK_TREE_VIEW(list),
   GTK_TREE_MODEL(store));

   g_object_unref(store);
   }


*/

void openuser(gchar *name)
{
	int i, j;
	int left, top;
	GtkWidget *image;

	for (i = 0; i < nusers; i++) {
		if (strcmp(users[i].name, name) == 0) {
			g_print("%s is at index %d\n", name, i);
			if (!users[i].isopen) {
				gtk_container_remove(GTK_CONTAINER(hbox), table);
				table = gtk_table_new(2, 2, TRUE);
				gtk_table_set_row_spacings(GTK_TABLE(table), 10);
				gtk_table_set_col_spacings(GTK_TABLE(table), 10);
				gtk_box_pack_start(GTK_BOX(hbox), table, FALSE, FALSE, 5);
				for (j = 0; j < NFEEDS; j++) {
					if (feeds[j] == i)
						break;

					if (feeds[j] < 0) {
						feeds[j] = i;
						break;
					}
				}
				for (j = 0; j < NFEEDS; j++) {
					if (feeds[j] < 0) {
						left = j %2;
						top = j / 2;
						blankimage = gtk_image_new_from_file("blank.jpeg");
						gtk_table_attach_defaults(GTK_TABLE(table), blankimage, left, left+1, top, top+1);
						gtk_widget_show(blankimage);
					} else {
						left = j % 2;
						top = j / 2;
						image = gtk_image_new_from_file("plan9vt220-tiny.jpeg");
						gtk_table_attach_defaults(GTK_TABLE(table), image, left, left+1, top, top+1);
						gtk_widget_show(image);
					}
				}
				gtk_widget_show(table);
			}
		}
	}
}


// Event handler for expose event
gboolean expose_cb1 (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
	//gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(data), GDK_WINDOW_XWINDOW(widget->window));
	//for (i = 0; i < NFEEDS; i++) {
	gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(sink1), GDK_WINDOW_XWINDOW(vids[0]->window));
	gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(sink2), GDK_WINDOW_XWINDOW(vids[1]->window));
	gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(sink3), GDK_WINDOW_XWINDOW(vids[2]->window));

	//		gtk_widget_show_all(vids[0]);
	//		gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(data), GDK_WINDOW_XWINDOW(vids[1]->window));
	//			gtk_widget_show_all(vids[1]);
	//	}
	gtk_widget_show_all(widget);
	printf("In expose event\n");
	return TRUE;
}




void init_gui(int argc, char *argv[])
{
	int i, left,top;
	// Make a window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	window2 = gtk_window_new(GTK_WINDOW_POPUP);	

	gtk_window_set_title(GTK_WINDOW(window), "Server UI");

	gtk_window_set_title(GTK_WINDOW(window2), "Feed 1");
	// MStump: Refuses to compile with the line below, probably something stupid 
	// 	   I'm missing.  Non-vital, works fair as is.
	//	g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), NULL);

	gtk_container_set_border_width(GTK_CONTAINER(window), 10);

	gtk_container_set_border_width(GTK_CONTAINER(window2), 0);

	g_signal_connect(G_OBJECT(window),"expose-event",G_CALLBACK(expose_cb1),sink2);

	gtk_widget_set_size_request(window2,320,240);
	/* Initialize boxes */
	hbox = gtk_hbox_new (FALSE, 0);
	vbox = gtk_vbox_new(FALSE, 0);

	/* Vbox goes in hbox, which goes in the window */
	gtk_container_add(GTK_CONTAINER(window), hbox);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 5);

	/* Make a table for the pretty pictures */
	table = gtk_table_new(2, 2, TRUE);
	gtk_table_set_row_spacings(GTK_TABLE(table), 10);
	gtk_table_set_col_spacings(GTK_TABLE(table), 10);
	gtk_box_pack_start(GTK_BOX(hbox), table, FALSE, FALSE, 5);

	/* Make a title for the user list */
	userlabel = gtk_label_new("Users");
	gtk_label_set_justify(GTK_LABEL(userlabel), GTK_JUSTIFY_CENTER);
	gtk_box_pack_start(GTK_BOX(vbox), userlabel, FALSE, FALSE, 5);
	gtk_widget_show(userlabel);

	/* Create a list for usernames */
	list = gtk_clist_new(2);
	gtk_clist_set_selection_mode(GTK_CLIST(list),GTK_SELECTION_SINGLE);
	gtk_clist_set_shadow_type(GTK_CLIST(list), GTK_SHADOW_NONE);
        gtk_clist_set_column_justification(GTK_CLIST(list),0,GTK_JUSTIFY_LEFT);
	gtk_clist_set_column_width(GTK_CLIST(list),0,100);


	///	list = gtk_tree_view_new();
	///	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(list), FALSE);

	/* Pack it into a box */
	gtk_box_pack_start(GTK_BOX(vbox), list, TRUE, TRUE, 0);

	///	init_list(list);

	// Format is Username - ID      
	gchar *names[4][2] = {  {(gchar*)"John Floren",(gchar*) "0"},
		{(gchar*)"Daniel Liu",(gchar*) "1"},
		{(gchar*)"Mark Stump",(gchar*) "2"}};

	int indx;
	for (indx = 0; indx < 4; indx++)
	{
		gtk_clist_append((GtkCList *)list, names[indx]);
		// Adding data (userID) to retrieve
		gtk_clist_set_row_data((GtkCList *)list, indx, names[indx][1]);
	}


	/* This is static garbage that will need to be fetched from the server in the real client */
	users[0].name = (char *)"John Floren"; users[0].isopen = 0;
	users[1].name = (char *)"Dan Liu"; users[1].isopen = 0;
	users[2].name = (char *)"Mark Stump"; users[2].isopen = 0;

///	for (i = 0; i < nusers; i++) {
///		add_to_list(list, users[i].name);
///	}

///	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(list));

	/* Put some stuff in the table for now */
	/* I use 320x240 blank images to make things auto-scale correctly */
	for (i = 0; i < NFEEDS; i++) {
		left = i %2;
		top = i / 2;
		//		blankimage = gtk_image_new_from_file("blank.jpeg");
		//		gtk_table_attach_defaults(GTK_TABLE(table), blankimage, left, left+1, top, top+1);
		vids[i] = gtk_drawing_area_new();
		gtk_widget_set_size_request(vids[i], 320, 240);
		gtk_table_attach_defaults(GTK_TABLE(table), vids[i], left, left+1, top, top+1);
		if (i == 0)
			gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(sink1), GDK_WINDOW_XWINDOW(vids[i]->window));
		//gtk_widget_show(blankimage);
	}

	g_signal_connect(list, "row-activated",  G_CALLBACK(on_doubleclick), NULL);

	g_signal_connect(G_OBJECT (window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

	gtk_widget_show(table);
	gtk_widget_show(list);
	gtk_widget_show(hbox);
	gtk_widget_show(vbox);

	gtk_widget_show(window);
	//	gtk_widget_show(window2);
	//	gtk_main();

}

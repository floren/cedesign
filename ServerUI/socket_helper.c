/*
 * socket_helper.c
 *
 *  Created on: Oct 24, 2009
 *      Author: daliu
 */
#include "socket_helper.h"

int Readline(int sockd, void *vptr, int maxlen)
{
	int n, rc;
	char c, *buffer;

	buffer = (char *)vptr;

	for (n = 1; n < maxlen; n++) {
		if ((rc = read(sockd, &c, 1)) == 1) {
			*buffer++ = c;
			if (c == '\n')
				break;
		} else if (rc == 0) {
			if (n == 1)
				return 0;
			else
				break;
		} else {
			if (errno = EINTR)
				continue;
			return -1;
		}
	}
	*buffer = 0;
	return n;
}

int Writeline(int sockd, const void *vptr, int n) {
	int nleft, nwritten;
	const char *buffer;

	buffer = (char *)vptr;
	nleft = n;

	while (nleft > 0) {
		if ((nwritten = write(sockd, buffer, nleft)) <= 0) {
			if (errno == EINTR)
				nwritten = 0;
			else
				return -1;
		}
		nleft -= nwritten;
		buffer += nwritten;
	}
	return n;
}

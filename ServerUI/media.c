/*
 * media.c
 *
 *  Created on: Oct 25, 2009
 *      Author: daliu
 */

#include "media.h"

void initStreams()
{
	// Filter to tell that audio is audio
	GstCaps *caps;

	videoPipeline = gst_pipeline_new("videoPipeline");
	videoSrc = gst_element_factory_make("udpsrc","video-source"); // port 60000
	videoDec = gst_element_factory_make("jpegdec","decoder");
	videoFilter = gst_element_factory_make("ffmpegcolorspace","colorfilter");
	sink1 = gst_element_factory_make("ximagesink","imagesink");

	audioPipeline = gst_pipeline_new("audioPipeline");
	audioSrc = gst_element_factory_make("udpsrc","audio-source");
	audioDec1 = gst_element_factory_make("vorbisdec", "audio-dec1");
	audioConvert1 = gst_element_factory_make("audioconvert", "audio-convert1");
	audioSink1 = gst_element_factory_make("alsasink","audio-sink1");

	videoPipeline2 = gst_pipeline_new("videopipeline2");
	videoSrc2 = gst_element_factory_make("udpsrc","video-source2"); // port 60002
	videoDec2 = gst_element_factory_make("jpegdec","decoder2");
	videoFilter2 = gst_element_factory_make("ffmpegcolorspace","colorfilter2");
	sink2 = gst_element_factory_make("ximagesink", "imagesink2");

	videoPipeline3 = gst_pipeline_new("videopipeline3");
	videoSrc3 = gst_element_factory_make("udpsrc","video-source3"); // port 60004
	videoDec3 = gst_element_factory_make("jpegdec","decoder3");
	videoFilter3 = gst_element_factory_make("ffmpegcolorspace","colorfilter3");
	sink3 = gst_element_factory_make("ximagesink", "imagesink3");


	caps = gst_caps_new_simple("audio/x-raw-int",
			"rate", G_TYPE_INT, 44100,
			"channels", G_TYPE_INT,2,
			"endianness", G_TYPE_INT, 1234,
			"signed", G_TYPE_BOOLEAN, FALSE,
			"width", G_TYPE_INT, 16,
			"depth", G_TYPE_INT, 16,
			NULL);

	if (!videoPipeline)
	{
		fprintf(stderr, "GStreamer Error: Could not create pipeline\n");
	}
	if(!videoSrc || !sink1 || !videoFilter || !videoDec)
	{

		fprintf(stderr, "GStreamer Error: Could not create element\n");
	}

	if (!audioSrc || !audioDec1 || audioConvert1 || audioSink1)
	{
		fprintf(stderr, "GStreamer Error: Could not create Sound element\n");
	}
	//g_object_set(G_OBJECT(videoSrc), "uri", "udp://129.21.67.124", "port",60000,NULL);
	g_object_set(G_OBJECT(videoSrc), "port",60000,NULL);
	g_object_set(G_OBJECT(audioSrc), "port",60001,NULL);

	g_object_set(G_OBJECT(videoSrc2), "port",60002,NULL);

	g_object_set(G_OBJECT(videoSrc3), "port",60004,NULL);


	gst_bin_add_many(GST_BIN(videoPipeline), videoSrc, videoDec, videoFilter, sink1, NULL);
	gst_bin_add_many(GST_BIN(audioPipeline), audioSrc, audioDec1, audioConvert1, audioSink1, NULL);

	gst_bin_add_many(GST_BIN(videoPipeline2), videoSrc2, videoDec2, videoFilter2, sink2, NULL);

	gst_bin_add_many(GST_BIN(videoPipeline3), videoSrc3, videoDec3, videoFilter3, sink3, NULL);



	if(!gst_element_link_many(videoSrc,videoDec,videoFilter,sink1,NULL))
	{
		fprintf(stderr, "GStreamer Video Error: Element Link Failed\n");
	}

	/*if(!gst_element_link_filtered(audioSrc,audioDec1,caps))
	{
		fprintf(stderr, "GStreamer Audio Error: Element Link Failed\n");
	}*/
	if(!gst_element_link_many(audioSrc, audioDec1, audioConvert1, audioSink1,NULL)) {
		fprintf(stderr, "GStreamer Audio Error: Element Link Failed\n");
	}

	if(!gst_element_link_many(videoSrc2, videoDec2, videoFilter2, sink2,NULL)) {
		fprintf(stderr, "GStreamer Video Error: Element Link Failed\n");
	}

	if(!gst_element_link_many(videoSrc3, videoDec3, videoFilter3, sink3,NULL)) {
		fprintf(stderr, "GStreamer Video Error: Element Link Failed\n");
	}


	gst_element_set_state(videoPipeline, GST_STATE_PLAYING);
	gst_element_set_state(audioPipeline, GST_STATE_PLAYING);

	gst_element_set_state(videoPipeline2, GST_STATE_PLAYING);

	gst_element_set_state(videoPipeline3, GST_STATE_PLAYING);
}

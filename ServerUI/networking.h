/*
 * networking.h
 *
 *  Created on: Oct 25, 2009
 *      Author: daliu
 */

#ifndef NETWORKING_H_
#define NETWORKING_H_
#include <gtk/gtk.h>
#include <netdb.h>

#include "socket_helper.h"

const char* login_mesg = "login server";
const char* getlist_mesg = "getlist";
const char* getstream_mesg = "getstream"; // getstream [port num] [user name]


void* main_server_thread(void* ptr);

#endif /* NETWORKING_H_ */
